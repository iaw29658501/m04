   var objecte2=[];
    var file='';
    function ini() {
        file='dades/edatMitjanaEsperanVidaDistrictesBCN_1.json';
        executar_ordre(file);
    }

    function executar_ordre(file) {
        var json = new XMLHttpRequest;
        json.onreadystatechange = function() {
            if (json.readyState == 4 && json.status == 200 ) {
                ordre_executada(json);
            }
            if (json.status == 404) {
                console.log("File or resource not found error 404" )
            }
        };
        json.open("GET", file, true);
        json.send();
    }
    function ordre_executada(dades,ord) {
        objecte2=JSON.parse(dades.responseText);
        visualitza_dades();
        // visualitza_mod();
    }
    function visualitza_dades() {
        var t = "<table style='width:100%'>"
        t += '<tr><th>Barri</th><th>Esperança de Vida</th><th>Edad Mitjana</th></tr>';
       for ( var i=0; i < objecte2.length; i++) {
            t+=`<tr>
                    <td>` + objecte2[i].Barri.substring(2) + '</td>' + '<td>' ;

                        for ( var j = 0; j < objecte2[i].esperan_vida.length; j++ ) {
                            t += '<div>'  + objecte2[i].esperan_vida[j].anys + " : " +
                            objecte2[i].esperan_vida[j].valor + '</div>';
                        }

                    t +='</td>';
                    t += '<td> ';
                        for ( var j = 0; j < objecte2[i].edat_mitjana.length; j++ ) {
                            t += '<div>' + objecte2[i].edat_mitjana[j].anys + "<br/>"+
                                    'D:' + objecte2[i].edat_mitjana[j].valor.dones + "<br/>" +
                                    'H:' + objecte2[i].edat_mitjana[j].valor.homes +
                                  '</div>'
                        }
                    t += '</td>'

       }
      t += "</table>";
      $('body').html(t);
    }

	    $(window).load(ini());
