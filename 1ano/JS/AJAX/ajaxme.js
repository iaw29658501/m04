var archivo = ""
function executar_ordre_servidor(file) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      ordre_executada(xhr);
    }
    if (xhr.status == 404) {
      console.log("File or resource not found")
    }
  };
  xhr.open("GET", file, true);
  xhr.send();
}

  function ordre_executada(xml) {
    var x, i, xmlDoc, table;
    xmlDoc = xml.responseXML;
    table = "<tr><th>Site</th><th>Link</th></tr>";
    x = xmlDoc.getElementsByTagName("link");
    for (i = 0; i < x.length; i++) {
      table += "<tr><td>" +
        x[i].getElementsByTagName("titulo")[0].childNodes[0].nodeValue +
        "</td><td>" +
        x[i].getElementsByTagName("href")[0].childNodes[0].nodeValue +
        "</td></tr>";
    }
    document.getElementById("demo").innerHTML = table;
  }

function ini() {
  executar_ordre_servidor(archivo);
}
