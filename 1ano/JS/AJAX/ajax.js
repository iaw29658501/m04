/*function loadXMLDoc() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // myFunction(this);
    }
  };
  xmlhttp.open("GET", "cd_catalog.xml", true);
  xmlhttp.send();
}*/
/*
function myFunction(xml) {
  var x, i, xmlDoc, table;
  xmlDoc = xml.responseXML;
  table = "<tr><th>Artist</th><th>Title</th></tr>";
  x = xmlDoc.getElementsByTagName("CD");

  for (i = 0; i < x.length; i++) {
    table += "<tr><td>" +
      x[i].getElementsByTagName("ARTIST")[0].childNodes[0].nodeValue +
      "</td><td>" +
      x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue +
      "</td></tr>";
  }
  document.getElementById("demo").innerHTML = table;
}
*/
function executar_ordre_servidor(ordre) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status==200) {
      ordre_executada(xmlhttp);
    }
  };
  xmlhttp.open("GET", "links.xml", true);
  xmlhttp.send();
}

  function ordre_executada(xml) {
    var x, i, xmlDoc, table;
    xmlDoc = xml.responseXML;
    table = "<tr><th>Site</th><th>Link</th></tr>";
    x = xmlDoc.getElementsByTagName("link");
    for (i = 0; i < x.length; i++) {
      table += "<tr><td>" +
        x[i].getElementsByTagName("titulo")[0].childNodes[0].nodeValue +
        "</td><td>" +
        x[i].getElementsByTagName("href")[0].childNodes[0].nodeValue +
        "</td></tr>";
    }
    document.getElementById("demo").innerHTML = table;
  }

  function ini() {
    executar_ordre_servidor('Links.xml');
  }

}
