/*function loadXMLDoc() {
  var xhr = new xhrRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // myFunction(this);
    }
  };
  xhr.open("GET", "cd_catalog.xml", true);
  xhr.send();
}*/
/*
function myFunction(xml) {
  var x, i, xmlDoc, table;
  xmlDoc = xml.responseXML;
  table = "<tr><th>Artist</th><th>Title</th></tr>";
  x = xmlDoc.getElementsByTagName("CD");

  for (i = 0; i < x.length; i++) {
    table += "<tr><td>" +
      x[i].getElementsByTagName("ARTIST")[0].childNodes[0].nodeValue +
      "</td><td>" +
      x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue +
      "</td></tr>";
  }
  document.getElementById("demo").innerHTML = table;
}
*/
var mineralPosition=0;

function executar_ordre_servidor(file) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      ordre_executada(xhr);
    }
    if (xhr.status == 404) {
      console.log("File or resource not found")
    }
  };
  xhr.open("GET", file, true);
  xhr.send();
}

function ordre_executada(xhr) {
  var x, i, xmlDoc, table;
  xmlDoc = xhr.responseXML;
  table = "<table>";
  xml = xmlDoc.getElementsByTagName("mineral");
  i = mineralPosition;
    table += "<tr><td>" + xml[i].getElementsByTagName("nom")[0].childNodes[0].nodeValue + "</td>" +
      "<td>" + xml[i].getElementsByTagName("duressa")[0].childNodes[0].nodeValue + "</td>" +
      "<td>" + xml[i].getElementsByTagName("densitat")[0].childNodes[0].nodeValue + "</td>";
    try {
      table += "<td>" + xml[i].getElementsByTagName("composicio")[0].childNodes[0].nodeValue + "</td>";
    } catch (err) {
      table += "<td> unknown </td>";
    }
    table += "<td>" + xml[i].getElementsByTagName("color")[0].childNodes[0].nodeValue + "</td>";
    try {
      table += "<td>" + xml[i].getElementsByTagName("grup")[0].childNodes[0].nodeValue + "</td>";
    } catch (err) {
      table += "<td> Sin Grupo </td>";
    }
    try {
      table += "<td> <img class='mineral' src='" + xml[i].getElementsByTagName("foto")[0].childNodes[0].nodeValue + "'></img></td>"
    } catch (err) {
      table += "<td> Sin Imagen </td>"
    }
  table += "</table>";
  document.getElementsByClassName("mineral")[0].innerHTML = table;
}

function ini() {
  mineralPosition=0;
  executar_ordre_servidor("minerals_simple.xml");
}

function changeMineral(positionVar) {
  if (positionVar == 'next') {
    mineralPosition += 1;
  } else {
    mineralPosition -= 1;
  }
  executar_ordre_servidor("minerals_simple.xml");
}
