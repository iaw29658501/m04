/*
 * JS        08/11/2018
 *
 * Moviment de peces
 *
 * Copyright 2018 Pablo Hernando <gayarre.pablo@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */
 // Inicialització de variables globals
 var mare;
 var nombreElementsMare = 0;
 var element;
 var x;
 var y;
 // Assignació de valors a les variables globals
 function ini() {
   mare = document.getElementById("esquerra");
   x = document.getElementById("x");
   x.value = 0;
   y = document.getElementById("y");
   y.value = 0;
 }
 function mostraCapsa(clase) {
   nombreElementsMare > 0 ? element.style.border = "none": false;
   mare.innerHTML += "<div id='c"+nombreElementsMare+"' class='"+clase+" capsa' onclick='select(this)'></div>";
   element = document.getElementById('c'+nombreElementsMare);
   element.style.border = "2px solid white";
   nombreElementsMare++;
 }
 function select(canviElement) {
   element.style.border = "none";
   element.style.zIndex = 0;
   element = canviElement;
   element.style.border = "2px solid white";
   element.style.zIndex = 1;
 }
 // Moviments
 function dreta() {
   x.value = element.offsetLeft;
   x.value = eval(parseInt(x.value) + 11);
   element.style.left = parseInt(x.value) + "px";
 }
 function esquerra() {
   x.value = element.offsetLeft;
   x.value = eval(parseInt(x.value) - 11);
   element.style.left =  parseInt(x.value) + "px";
 }
 function abaix() {
   y.value = element.offsetTop;
   y.value = eval(parseInt(y.value) + 11);
   element.style.top = parseInt(y.value) + "px";
 }
 function adalt() {
   y.value = element.offsetTop;
   y.value = eval(parseInt(y.value) - 11);
   element.style.top = parseInt(y.value) + "px";
 }
