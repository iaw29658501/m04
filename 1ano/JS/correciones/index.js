window.addEventListener("load", ini);

var lletraSel;
var numErrors = 0;
var paraula = "DESEMBOLICAR";

function ini() {
  lletraSel = 0;
  numErrors = 0;
  lletraSel = document.getElementById("sel-lletra");
  var errors = document.getElementById("numErrors").innerHTML = numErrors;
  // Bucle per posar les lletres a display none.
  for (var i = 1; i <= 12; i++) {
    document.getElementById("l" + i).style.display = "none";
    console.log("lletra tapada")
  }
}

function introduirLletra() {
  lletraSel = document.getElementById("sel-lletra").value.toUpperCase();
  console.log("Has escollit la " + lletraSel);
  var trobat = false;
  for (var i = 1; i <= paraula.length; i++) {
    if (lletraSel == paraula.substr(i - 1, 1)) {
      document.getElementById("l" + i).style.display = "block";
      console.log("letra encertada");
      trobat = true;
    }
  }
  if (!trobat) {
    numErrors++;
    errors = document.getElementById("numErrors").innerHTML = numErrors;
    console.log("error");
    cert = false;
  }
}

function setSecret() {
	paraula = document.getElementById('secretSetter').value.toUpperCase();
  if (paraula.length <= 12) {
    for (var i = 1; i < paraula.length; i++) {
      document.getElementById('l' + i).innerHTML = paraula[i - 1];
    }
    for (var i = paraula.length; i <= 12; i++) {
      document.getElementById('l' + i).innerHTML = '';
    }
  }
}
// Error: mostra la lletra anterior a la cual tendria que ser

// Funció per veure si la lletra forma part de la paraula.
