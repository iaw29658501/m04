/*
 * JS        20/03/2019
 *
 * Poker Mentiroso
 *
 * Copyright 2018 Pablo Hernando <gayarre.pablo@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */
 
var diners = 100;
var daus = ['7','8', 'J', 'Q', 'K', 'A'];
var dinersInput;
var queTens = 'Bosc';
var queTensInput;
var caselles = 5;
var mare;
var dausCaselles;
var moviments = 3;
var monedesGuanyades = 0;

function ini() 
{
	moviments = 3;
	monedesGuanyades = 0;
	queTens = 'Bosc';
	dinersInput = document.getElementById('diners');
	dinersInput.value = diners;
	queTensInput = document.getElementById('queTens');
	queTensInput.value = queTens;
	movimentsInput = document.getElementById('moviments');
	movimentsInput.value = moviments;
	mare = document.getElementById('mare');
	dibuixarCaselles();
	dausCaselles = document.getElementsByClassName('capsaDau');
	document.getElementById('jugar').innerHTML = '<button onclick="comencaJoc()">Jugar</button>';
}

function dibuixarCaselles() 
{
	divs = "";
	for(i=0; i<caselles; i++) {
		divs += "<div class='capsaDau'></div>";
	}
	mare.innerHTML = divs;
}

function getRandomPosicio() 
{
	return Math.floor(Math.random() * caselles) + 1;
}

function comencaJoc() 
{
	diners -= 10;
	dinersInput.value = diners;
	for(i=0; i<caselles; i++) 
	{
		img = daus[getRandomPosicio()];
		dausCaselles[i].style.backgroundImage = 'url("img/dau'+daus[getRandomPosicio()]+'.png")';
		dausCaselles[i].style.backgroundColor = 'black';
		dausCaselles[i].setAttribute("onclick", 'marcar(this, '+i+')');
	}
	document.getElementById('jugar').innerHTML = '<button onclick="moviment()">Cambiar</button><button onclick="acaba()">Acabar</button>';
	calculaTens();
	
}
function marcar(dauCasella, posicio) 
{
	if (dauCasella.style.border == '2px solid green') 
	{
		dauCasella.style.border = 'none';
	}
	else 
	{
		dauCasella.style.border = '2px solid green';
	}

}
function moviment()
{
	moviments--;
	movimentsInput.value = moviments;
	
	for (i = 0; i < caselles; i++) 
	{
		if (dausCaselles[i].style.border == '2px solid green') 
		{
			img = daus[getRandomPosicio()];
			dausCaselles[i].style.backgroundImage = 'url("img/dau'+img+'.png")';
			dausCaselles[i].style.backgroundColor = 'black';
			dausCaselles[i].setAttribute("onclick", 'marcar(this, '+i+')');
		}
	}
	calculaTens();
	if (moviments == 0) 
	{
		acaba();
	}
}

function calculaTens() {
	tens = "bosc";
	parelles = 0;
	trios = false;
	poker = false;
	escalera = false;
	repoker = false;
	
	for (i = 0; !poker && !repoker && i < caselles; i++) {
		repetit=0;
		actual = dausCaselles[i];
		actual.style.backgroundColor = 'white';
		for (j = i; j < caselles; j++) {
			comparant = dausCaselles[j];
			if (comparant.style.backgroundColor != 'white' &&
				comparant.style.backgroundImage == actual.style.backgroundImage) {
				repetit++;
				comparant.style.backgroundColor = 'white';
			}
		}
		repetit == 1 ? parelles++ : false;
		repetit == 2 ? trios = true : false;
		repetit == 3 ? poker = true : false;
		repetit == 4 ? repoker = true : false;
	}
	parelles == 2 ? (tens = "doble parella", monedesGuanyades = 5) : false;
	trios  ? (tens = "trio", monedesGuanyades = 10) : false;
	trios && parelles == 1 ? (tens = "full", monedesGuanyades = 15) : false;
	poker ? (tens = "poker", monedesGuanyades = 20) : false;
	repoker ? (tens = 'repoker', monedesGuanyades = 25) : false;
	queTensInput.value = tens;
	treuEstils();
}

function treuEstils()
{
	for (i = 0; i < caselles; i++) {
		dausCaselles[i].style.backgroundColor = 'black';
		dausCaselles[i].setAttribute("onclick", 'marcar(this, '+i+')');
		dausCaselles[i].style.border = 'none';
	}
	
}
function acaba()
{
	diners += monedesGuanyades;
	dinersInput.value = diners;
	alert("Tens" + tens + ". Monedes guanyades " + monedesGuanyades );
	ini();
	
}
