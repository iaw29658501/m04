var objectiveLetter;
var i = 0;
document.addEventListener("DOMContentLoaded", init);
let par = document.getElementById("parent")
function init() {
    window.addEventListener("keyup", function keypressed(e) {
        checkIfLetter(e.key.toLocaleUpperCase())
    });
    changeObjectiveLetter();
}

function randomLetter() {
    let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let position = Math.floor(Math.random() * charset.length)
    return charset.charAt(position);
}

function changeObjectiveLetter() {
    objectiveLetter = randomLetter();
    document.getElementById("objective").innerHTML = objectiveLetter;
}

function checkIfLetter(letter) {
    if ( letter.match(/^[A-Z]$/i)) {
        letterPressed(letter);
    }
}

function letterPressed(letter) {
    console.log("pressed " + letter + " : " + objectiveLetter)
    document.getElementById("pressed").innerHTML = letter;
    if ( letter.localeCompare(objectiveLetter.toUpperCase()) == 0 ) {
        changeScore(1)
    } else {
        changeScore(-1)
    }
    changeObjectiveLetter();
}

function changeScore(delta) {
    let score = document.getElementById("score").innerHTML;
    score = parseInt(score);
    score+=delta;
    document.getElementById("score").innerHTML=score;
    i=0;
}


function newLetter() {
    changeObjectiveLetter();
    i++;
    if ( i > 1) {
        alert("game over");
        window.location.reload();
       
    }
}