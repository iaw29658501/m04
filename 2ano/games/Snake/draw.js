//DISPLAY VARIABLES
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
// COLOR
const CANVAS_BORDER_COLOUR = 'black';
const CANVAS_BACKGROUND_COLOUR = "white";
const SNAKE_COLOUR = 'lightgreen';
const SNAKE_BORDER_COLOUR = 'darkgreen';
const FOOD_COLOUR = 'red';
const FOOD_BORDER_COLOUR = 'darkred';

function drawWalls() {
    context.fillStyle = 'grey';
    for (let i = 0; i < walls.length; i++) {
        let x = walls[i].x;
        let y = walls[i].y;
        context.fillRect(x * BOX, y * BOX, BOX - 1, BOX - 1);
    }
}

function displayPaused() {
    context.globalAlpha = 0.6;
    context.fillStyle = 'black';
    context.fillRect(0, 0, DIMENSION * BOX, DIMENSION * BOX);
    context.globalAlpha = 1;
}

function displayAll() {
    //clear screen
    context.fillStyle = 'white';
    context.fillRect(0, 0, canvas.width, canvas.height);
    // draw food
    drawWalls();
    context.fillStyle = 'red';
    context.fillRect(food.x * BOX, food.y * BOX, BOX - 1, BOX - 1);
    // draw snake one cell at a time
    context.fillStyle = 'green';
    snake.cells.forEach(function (cell, index) {
        // drawing 1 px smaller than the grid creates a grid effect in the snake body so you can see how long it is
        context.fillRect(cell.x * BOX, cell.y * BOX, BOX - 1, BOX - 1);
    }
    );
    // this variable is to not alowing the usere to change 90 degres then 90 degrees (180) from one frame to the other bypassing the other 90 degrees control
    oneMovePerFrameControl = false;
    document.getElementById("energybar").value = energy;
}

function drawBiggerMana() {
    document.getElementById("energybar").max = (level / 10 + 1) * snake.energyCapacity
}