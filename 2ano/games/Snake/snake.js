// GRID SIZE
const BOX = 10;

// VARIABLES THAT HANDLE THE DIFFICULTY
const GAME_SPEED = 80;
const INITIAL_ENERGY = 100;
const DIMENSION = Math.floor(canvas.width / BOX)

// BUTTONS | CONTROL

var KEY_LEFT = 37;
var KEY_UP = 38;
var KEY_RIGHT = 39;
var KEY_DOWN = 40;
document.addEventListener("keydown", function (e) {
    // You cannot move 2 times in less then one frame or you could change
    // from up to down without the next frame of goins to the sides
    if (!oneMovePerFrameControl) {
        if (e.which == KEY_LEFT && snake.dx === 0) {
            snake.dx = -1;
            snake.dy = 0;
            oneMovePerFrameControl = true
        } else if (e.which == KEY_RIGHT && snake.dx === 0) {
            snake.dx = 1;
            snake.dy = 0;
            console.log("right")
            oneMovePerFrameControl = true
        } else if (e.which == KEY_UP && snake.dy === 0) {
            snake.dx = 0;
            snake.dy = -1;
            oneMovePerFrameControl = true
        } else if (e.which == KEY_DOWN && snake.dy === 0) {
            snake.dx = 0;
            snake.dy = 1;
            oneMovePerFrameControl = true
        } else if (e.code == "Space") {
            paused = !paused;
            displayPaused();
            /* if (paused) {
                 alert("Game paused, press space to get back")
             }*/
        }
    }
});
// GAME VARIABLES
var matrix = createMatrix();
var energy = INITIAL_ENERGY;
var food = {
    x: 0,
    y: 0
};
var maxenergy = 100;
// walls will be a list with pair of coordenated for each wall obstacle
var walls = []
var score = 0;
var dinamicObstacles = []

/*
class Snake {
    constructor () {
        this.x = 16;
        this.y=16;
        this.dx=1;
        this.dy=0;
        this.cells = [ { x: this.x, y:this.y  } ];
        this.maxCells:4;
    }
}
*/
var snake = {
    //head positions
    x: 16,
    y: 16,

    // snake velocity. moves one grid length every frame in either the x or y direction
    dx: 1,
    dy: 0,

    // keep track of all grids the snake body occupies
    cells: [{ x: 16, y: 16 }],

    // length of the snake. grows when eating an food
    maxCells: 3
};
newFood();
var paused = false;

window.addEventListener("load", function () {
    newGame()
})

function newGame() {

    makeWalls();
    newFood()
    score = 0;
    setInterval(function () {
        if (!paused) {
            // Logic part in this function
            snakeTick();
            // Display on screen
            displayAll();
        }
    }, GAME_SPEED)
}

function createMatrix() {
    let matrix = new Array(DIMENSION)
    for (let i = 0; i < matrix.length; i++) {
        matrix[i] = new Array(DIMENSION)
        for (let j = 0; j < matrix[i].length; j++) {
            matrix[i][j] = 0;
        }
    }
    return matrix;
}

matrixTranlation = {
    0: "free",
    1: "snake",
    2: "food",
    3: "wall",
    4: "obstacle"
}

function displayPaused() {
    context.globalAlpha = 0.6;
    context.fillStyle = 'black';
    context.fillRect(0, 0, DIMENSION * BOX, DIMENSION * BOX);
    context.globalAlpha = 1;
}


// TODO : Change for only display for food and snake tail (changed boxes)
function displayAll() {
    //clear screen
    context.clearRect(0, 0, canvas.width, canvas.height);
    // draw food
    drawWalls();
    context.fillStyle = 'red';
    context.fillRect(food.x * BOX, food.y * BOX, BOX - 1, BOX - 1);
    // draw snake one cell at a time
    context.fillStyle = 'green';
    snake.cells.forEach(function (cell, index) {
        // drawing 1 px smaller than the grid creates a grid effect in the snake body so you can see how long it is
        context.fillRect(cell.x * BOX, cell.y * BOX, BOX - 1, BOX - 1);
    }
    );
    // this variable is to not alowing the usere to change 90 degres then 90 degrees (180) from one frame to the other bypassing the other 90 degrees control
    oneMovePerFrameControl = false;
    document.getElementById("energy").value = energy;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
function gameOver() {
    // saving scores
    storeLocalRecord();
    window.location.reload(false);
}

function storeLocalRecord() {
    let scores = localStorage.getItem("scores")
    if (scores != null) {
        scores = JSON.parse(scores)
    }
    if (!Array.isArray(scores)) {
        scores = []
    }
    let name = window.prompt("Whats your username?")
    let score = parseInt(document.getElementById("score").innerHTML)
    record = { name: name, score: score };
    scores.unshift(record);
    scores.sort(function (a, b) {
        return b.score - a.score;
    })

    localStorage.setItem("scores", JSON.stringify(scores));
}

function newFood() {
    food.x = getRandomInt(0, DIMENSION);
    food.y = getRandomInt(0, DIMENSION);
    if (!isPositionFree(food.x, food.y)) {
        newFood();
    } else {
        matrix[food.x][food.y] = 2;
    } 
}

// TODO
function isPositionFree(x, y) {
    for (let i = 0; i < snake.cells.length; i++) {
        if (snake.cells[i].x == x && snake.cells[i].y == y) {
            return false;
        }
    }
    for (let i = 0; i < walls.length; i++) {
        if (walls[i].x == x && walls[i].y == y) {
            return false;
        }
    }
    for (let i = 0; i < dinamicObstacles.length; i++) {
        if (dinamicObstacles[i].x == x && dinamicObstacles[i].y == y) {
            return false;
        }
    }


    return true;
}

function snakeTick() {
    energy--;
    document.getElementById("energybar").value = energy;
    if (energy <= 0) {
        gameOver();
        return;
    }
    snake.x += snake.dx;
    snake.y += snake.dy;
    // wrap snake position horizontally on edge of screen
    if (snake.x < 0) {
        snake.x = DIMENSION - 1;
    }
    else if (snake.x >= DIMENSION) {
        snake.x = 0;
    }
    // wrap snake position vertically on edge of screen
    if (snake.y < 0) {
        snake.y = DIMENSION - 1;
    }
    else if (snake.y >= DIMENSION) {
        snake.y = 0;
    }

    snake.cells.unshift({ x: snake.x, y: snake.y });
    
    // is the head in a food box?
    let gotFood = snake.x == food.x && snake.y == food.y;
    if (gotFood) {
        // POINTS !!
        energy += 30;
        if (energy > maxenergy) {
            energy = maxenergy;
        }
        let score = parseInt(document.getElementById("score").innerHTML)
        // The bigger the snake the more
        document.getElementById("score").innerHTML = score + 10;
        snake.maxCells++;
        newFood();
        if (score % 50 == 0) {
            newObstacle();
        }
    }

    if (snake.maxCells < snake.cells.length) {
        snake.cells.pop()
    }
    // TODO: change for isPositionFree on the head coordinates
    // CHECK IF SNAKE EATED HERSELF
    snake.cells.forEach(function (cell, index) {
        // check collision with all cells after this one (modified bubble sort)
        for (var i = index + 1; i < snake.cells.length; i++) {
            // snake occupies same space as a body part. reset game
            if (cell.x === snake.cells[i].x && cell.y === snake.cells[i].y) {
                gameOver();
            }
        }
    });
    let headPosition = matrix[snake.x][snake.y]
    if (headPosition == 3 || headPosition == 4) {
        gameOver();
    }
}

// OBSTACULOS
function makeWalls() {
    // Getting a random size for the wall
    let sizes = [Math.floor(DIMENSION / 4), Math.floor(DIMENSION / 2)]
    let randomIndex = getRandomInt(0, sizes.length)
    let size = sizes[randomIndex]

    // Making the walls size proporsional to the board (both even or both pair)
    if (DIMENSION % 2 != size % 2) {
        size++;
    }
    // Creating random map 
    let positions = ["vertical", "horizontal", "both"]
    randomIndex = getRandomInt(0, positions.length)
    let position = positions[randomIndex]

    if (position == "vertical" || position == "both") {
        let randomPos = getRandomInt(0, DIMENSION);
        let randomPos2 = getRandomInt(0, DIMENSION);
        for (let i = 1; i <= size; i++) {
            let freeSpace = (DIMENSION - size) / 2;
            matrix[randomPos2][freeSpace + i] = 3;
            matrix[DIMENSION - randomPos][freeSpace + i] = 3;
            walls.push({ x: randomPos2, y: freeSpace + i });
            walls.push({ x: DIMENSION - randomPos, y: freeSpace + i });
        }
    }
    if (position == "horizontal" || position == "both") {
        let randomPos = getRandomInt(0, DIMENSION);
        let randomPos2 = getRandomInt(0, DIMENSION);
        for (let i = 1; i <= size; i++) {
            let freeSpace = (DIMENSION - size) / 2;
            matrix[freeSpace + i][randomPos] = 3;
            matrix[freeSpace + i][DIMENSION - randomPos2] = 3;
            walls.push({ x: freeSpace + i, y: randomPos });
            walls.push({ x: freeSpace + i, y: DIMENSION - randomPos2 });
        }
    }
    drawWalls();
}

function newObstacle(num = 2) {
    // pair or coordenates with free space
    //for (let i = 0; i < 2; i++) {
    let xy = getFreePosition();
    let x = xy[0];
    let y = xy[1];
    matrix[x][y] = 4;
    // }
    clearObstaclesFromMatrix();
    dinamicObstacles = []
    dinamicObstacles.push({ x: x, y: y })


    xy = getFreePosition();
    x = xy[0];
    y = xy[1];
    matrix[x][y] = 4;
    // }
    dinamicObstacles.push({ x: x, y: y })

}


function clearObstaclesFromMatrix() {
    for (let i = 0;  i < dinamicObstacles.length; i++) {
        let obst = dinamicObstacles[i];
        let x = obst.x
        let y = obst.y
        matrix[x][y] = 0;
    }
}

function drawWalls() {
    context.fillStyle = 'grey';
    for (let i = 0; i < walls.length; i++) {
        let x = walls[i].x;
        let y = walls[i].y;
        context.fillRect(x * BOX, y * BOX, BOX - 1, BOX - 1);
    }

    //ading draw obstacles
    context.fillStyle = 'black';
    for (let i = 0; i < dinamicObstacles.length; i++) {
        let x = dinamicObstacles[i].x;
        let y = dinamicObstacles[i].y;
        context.fillRect(x * BOX, y * BOX, BOX - 1, BOX - 1);
    }
}

// Te devuelve un par de coordenadas [x,y] que este en vacia
function getFreePosition() {
    let pair = randomPair()
    let x = pair[0]
    let y = pair[1]
    while (!isPositionFree(x, y)) {
        pair = randomPair();
        x = pair[0]
        y = pair[1]
    }
    return pair;
}
// Te devuelve un par de coordenadar XY aleatorio
function randomPair() {
    let x = getRandomInt(0, DIMENSION);
    let y = getRandomInt(0, DIMENSION);
    return [x, y];
}
