let adios = ()=>{
    window.alert("Adiós!");
}

//comentario de 1 línea

/* comentarios 
de
varias
líneas
*/

// Variables
// pueden empezar con cualquier letra, o "_", o "$"
var a; var A;
var b, b;
//a != A

 console.log(a==A);
 console.log(typeof a);
 console.log(a);
 a="";
 console.log(typeof a);
 console.log(a);
 
 console.log(a==A);

 var numero1 = parseInt(window.prompt("Introduce un número"));
 console.log(numero1);
